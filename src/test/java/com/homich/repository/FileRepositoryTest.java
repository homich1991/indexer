package com.homich.repository;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class FileRepositoryTest extends FileRepository {

    public FileRepositoryTest() {
    }

    private FileRepository fileRepository;

    @Before
    public void beforeTest() {
        fileRepository = new FileRepository();
    }

    @Test
    public void storeFile() throws URISyntaxException {
        File file = new File(getClass().getResource("/file1.txt").toURI());
        fileRepository.storeFile(file);

        List<String> fileIdsByFileName = fileRepository.getFileIdsByFileName(List.of(file.getAbsolutePath()));

        assertEquals("Result should contain correct fileIds count", 1, fileIdsByFileName.size());
        assertFalse("Id should not be empty or blank", fileIdsByFileName.get(0).isBlank());
    }

    @Test
    public void storeSameFileSecondTime() throws URISyntaxException {
        File file = new File(getClass().getResource("/file1.txt").toURI());
        fileRepository.storeFile(file);

        fileRepository.storeFile(file);

        List<String> fileIdsByFileName = fileRepository.getFileIdsByFileName(List.of(file.getAbsolutePath()));

        assertEquals("Result should contain correct fileIds count", 1, fileIdsByFileName.size());
        assertFalse("Id should not be empty or blank", fileIdsByFileName.get(0).isBlank());
        assertEquals("Only one file should be in the storage", 1, fileRepository.fileStorage.size());
        assertEquals("Only one file should be in the file info storage", 1, fileRepository.fileNameToInfo.size());
    }

    @Test
    public void getIdsByFileNames() throws URISyntaxException {
        File file1 = new File(getClass().getResource("/file1.txt").toURI());
        String file1Id = fileRepository.storeFile(file1);
        File file2 = new File(getClass().getResource("/file2.txt").toURI());
        String file2Id = fileRepository.storeFile(file2);

        List<String> files = fileRepository.getAllFilesByIds(List.of(
                file1Id,
                file2Id));

        assertEquals("Result should contain correct file names count", 2, files.size());
        assertEquals("File names should be correct",
                List.of(file1.getAbsolutePath(), file2.getAbsolutePath()),
                files);
    }

    @Test
    public void getAllFilesInIndexTest() throws URISyntaxException {
        File file1 = new File(getClass().getResource("/file1.txt").toURI());
        fileRepository.storeFile(file1);
        File file2 = new File(getClass().getResource("/file2.txt").toURI());
        fileRepository.storeFile(file2);

        Set<String> fileNames = fileRepository.getAllFilesInIndex();

        assertEquals("Result should contain correct file names count", 2, fileNames.size());
        assertEquals("File names should be correct",
                Set.of(file1.getAbsolutePath(), file2.getAbsolutePath()),
                fileNames);
    }


    @Test
    public void removeFileTest() throws URISyntaxException {
        File file1 = new File(getClass().getResource("/file1.txt").toURI());
        String id = fileRepository.storeFile(file1);

        fileRepository.removeFiles(List.of(id));

        Set<String> fileNames = fileRepository.getAllFilesInIndex();

        assertEquals("Result should contain correct fileIds count", 0, fileNames.size());
        assertEquals("Only one file should be in the storage", 0, fileRepository.fileStorage.size());
        assertEquals("Only one file should be in the file info storage", 0, fileRepository.fileNameToInfo.size());

    }


}
