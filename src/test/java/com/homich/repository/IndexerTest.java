package com.homich.repository;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;


public class IndexerTest extends Indexer {

    public IndexerTest() {
    }

    private Indexer indexer;

    private HashMap<String, Set<String>> prepareWordToFilesMap() {
        HashMap<String, Set<String>> wordsToFilesMap = new HashMap<>();
        wordsToFilesMap.put("mama", new HashSet<>(Set.of("file1", "file2")));
        wordsToFilesMap.put("ramy", new HashSet<>(Set.of("file2", "file3")));
        return wordsToFilesMap;
    }

    @Before
    public void beforeTest() {
        indexer = new Indexer();
    }

    @Test
    public void addAndSearchForExistingWord() {
        HashMap<String, Set<String>> wordsToFilesMap = prepareWordToFilesMap();

        indexer.addToIndex(wordsToFilesMap);

        List<String> filesWithWord = indexer.findFilesIds("mama");
        filesWithWord.sort(String::compareTo);

        assertEquals("Result should contain correct fileIds", List.of("file1", "file2"), filesWithWord);
    }

    @Test
    public void addAndSearchForNonExistingWord() {
        HashMap<String, Set<String>> wordsToFilesMap = prepareWordToFilesMap();

        indexer.addToIndex(wordsToFilesMap);

        List<String> filesWithWord = indexer.findFilesIds("papa");

        assertEquals("Result should contain correct fileIds", Collections.emptyList(), filesWithWord);
    }

    @Test
    public void removeFromIndex() {
        HashMap<String, Set<String>> wordsToFilesMap = prepareWordToFilesMap();
        indexer.addToIndex(wordsToFilesMap);

        indexer.removeFiles(List.of("file1"));

        List<String> filesWithWord = indexer.findFilesIds("mama");

        assertEquals("Result should contain correct fileIds", List.of("file2"), filesWithWord);
    }

    @Test
    public void removeAllFilesWithWordFromIndex() {
        HashMap<String, Set<String>> wordsToFilesMap = prepareWordToFilesMap();
        indexer.addToIndex(wordsToFilesMap);

        indexer.removeFiles(new ArrayList<>(wordsToFilesMap.get("mama")));

        List<String> filesWithWord = indexer.findFilesIds("mama");

        assertEquals("Result should contain correct fileIds", Collections.emptyList(), filesWithWord);
    }

    @Test
    public void addToExistingIndex() {
        HashMap<String, Set<String>> wordsToFilesMap = prepareWordToFilesMap();
        indexer.addToIndex(wordsToFilesMap);

        wordsToFilesMap = new HashMap<>();
        wordsToFilesMap.put("mama", new HashSet<>(Set.of("file3")));

        indexer.addToIndex(wordsToFilesMap);

        List<String> filesWithWord = indexer.findFilesIds("mama");
        filesWithWord.sort(String::compareTo);

        assertEquals("Result should contain correct fileIds", List.of("file1", "file2", "file3"), filesWithWord);
    }

    @Test
    public void addToExistingIndexSameHashes() {
        HashMap<String, Set<String>> wordsToFilesMap = new HashMap<>();
        wordsToFilesMap.put("11", new HashSet<>(Set.of("file1", "file2")));
        indexer.addToIndex(wordsToFilesMap);

        wordsToFilesMap = new HashMap<>();
        wordsToFilesMap.put("22", new HashSet<>(Set.of("file3")));

        indexer.addToIndex(wordsToFilesMap);

        List<String> filesWithWord = indexer.findFilesIds("11");
        filesWithWord.sort(String::compareTo);

        assertEquals("Result should contain correct fileIds", List.of("file1", "file2"), filesWithWord);
    }


}
