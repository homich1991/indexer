package com.homich.service;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class IndexPreparationServiceTest {

    private final IndexPreparationService indexPreparationService = new IndexPreparationService();

    @Test
    public void prepareIndexForFile() throws URISyntaxException {
        File file = new File(getClass().getResource("/file1.txt").toURI());
        Map<String, Set<String>> wordsToFilesMap = indexPreparationService.prepareIndexForFile("file1", file);

        assertEquals("Result should contain correct fileIds count", 3, wordsToFilesMap.size());
        assertEquals("Result map should be correct",
                Map.of("mama", Set.of("file1"),
                        "mila", Set.of("file1"),
                        "ramy", Set.of("file1")),
                wordsToFilesMap);
    }

    @Test
    public void prepareIndexForFiles() throws URISyntaxException {
        File file1 = new File(getClass().getResource("/file1.txt").toURI());
        File file2 = new File(getClass().getResource("/file2.txt").toURI());
        Map<String, Set<String>> wordsToFilesMap = indexPreparationService.prepareIndexForFiles(
                List.of(new ImmutablePair<>("file1", file1),
                        new ImmutablePair<>("file2", file2)
                )
        );

        assertEquals("Result should contain correct fileIds count", 5, wordsToFilesMap.size());
        assertEquals("Result map should be correct",
                Map.of("mama", Set.of("file1"),
                        "mila", Set.of("file1"),
                        "ramy", Set.of("file1", "file2"),
                        "papa", Set.of("file2"),
                        "pila", Set.of("file2")),
                wordsToFilesMap);
    }
}
