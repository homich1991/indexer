package com.homich.service;

import com.homich.entity.FileInfo;
import com.homich.repository.FileRepository;
import com.homich.repository.Indexer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceTest {

    private FileService fileService;

    @Mock
    private FileRepository fileRepository;
    @Mock
    private IndexPreparationService indexPreparationService;
    @Mock
    private Indexer indexer;
    @Mock
    private AppEventHandler appEventHandler;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        fileService = new FileService(fileRepository, indexPreparationService, indexer, appEventHandler);
    }

    @Test
    public void indexFile() {
        when(fileRepository.storeFile(any(File.class))).thenReturn("file1");
        when(indexPreparationService.prepareIndexForFile(anyString(), any(File.class))).thenReturn(new HashMap<>());
        doNothing().when(indexer).addToIndex(any());


        fileService.indexFile(new File(""));

        verify(fileRepository).storeFile(any());
        verify(indexPreparationService).prepareIndexForFile(anyString(), any(File.class));
        verify(indexer).addToIndex(any());
    }

    @Test
    public void indexFiles() {
        when(fileRepository.storeFile(any(File.class))).thenReturn("fileId");
        when(indexPreparationService.prepareIndexForFiles(any())).thenReturn(new HashMap<>());
        doNothing().when(indexer).addToIndex(any());


        fileService.indexFiles(List.of(new File("file1"), new File("file2")));

        verify(fileRepository, times(2)).storeFile(any());
        verify(indexPreparationService).prepareIndexForFiles(any());
        verify(indexer).addToIndex(any());
    }


    @Test
    public void checkChanges() {
        String basePath = new File("").getAbsolutePath() + File.separator;

        File file1 = new File("file1");
        FileInfo file1Info = new FileInfo("file1", file1.getAbsolutePath(), "1");

        File file2 = new File("file2");

        File file3 = new File("file3");
        FileInfo file3Info = new FileInfo("file3", file3.getAbsolutePath(), "3");

        Set<String> filesInIndex = new HashSet<>(Set.of(
                basePath + "file1",
                basePath + "file3",
                basePath + "file4"
        ));


        when(fileRepository.getFileInfo(file1.getAbsolutePath())).thenReturn(file1Info);
        when(fileRepository.calculateMd5(file1)).thenReturn("1");

        when(fileRepository.getFileInfo(file3.getAbsolutePath())).thenReturn(file3Info);
        when(fileRepository.calculateMd5(file3)).thenReturn("4");

        FileService.ChangesBetweenFSAndIndex changes = fileService.checkChanges(List.of(file1, file2, file3), filesInIndex);

        assertEquals("Correct number of files should be added to index", 1, changes.getCreated().size());
        assertEquals("Correct files should be added to index", List.of(file2), changes.getCreated());
        assertEquals("Correct number of files should be updated in index", 1, changes.getUpdated().size());
        assertEquals("Correct files should be updated in index", List.of(file3), changes.getUpdated());
        assertEquals("Correct number of files should be deleted from index", 1, changes.getDeleted().size());
        assertEquals("Correct files should be deleted from index", Set.of(basePath + "file4"), changes.getDeleted());

    }

}
