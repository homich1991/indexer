package com.homich.service;

import com.homich.repository.FileRepository;
import com.homich.repository.Indexer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IndexSearchServiceTest extends IndexSearchService {

    private IndexSearchService indexSearchService;

    @Mock
    private Indexer indexer;
    @Mock
    private FileRepository fileRepository;

    public IndexSearchServiceTest() {
        super(null, null);
    }

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        indexSearchService = new IndexSearchService(indexer, fileRepository);
    }

    @Test
    public void searchByOneWord() {
        when(indexer.findFilesIds(anyString())).thenReturn(List.of("file1"));
        when(fileRepository.getAllFilesByIds(anyCollection())).thenReturn(List.of("file1Name"));


        List<String> fileNames = indexSearchService.searchBy("mama");

        assertEquals("Result should contain correct fileNames count", 1, fileNames.size());
        assertEquals("Result list should be correct",
                List.of("file1Name"), fileNames);

        verify(indexer).findFilesIds(anyString());
        verify(fileRepository).getAllFilesByIds(anyCollection());
    }

    @Test
    public void searchByOneWordWithSpaces() {
        when(indexer.findFilesIds(anyString())).thenReturn(List.of("file1"));
        when(fileRepository.getAllFilesByIds(anyCollection())).thenReturn(List.of("file1Name"));


        List<String> fileNames = indexSearchService.searchBy("   mama   ");

        assertEquals("Result should contain correct fileNames count", 1, fileNames.size());
        assertEquals("Result list should be correct",
                List.of("file1Name"), fileNames);
        verify(indexer).findFilesIds(anyString());
        verify(fileRepository).getAllFilesByIds(anyCollection());
    }

    @Test
    public void searchByMultupleWordsWithSpaces() {
        when(indexer.findFilesIds("mama")).thenReturn(List.of("file1"));
        when(indexer.findFilesIds("mila")).thenReturn(List.of("file1", "file2"));
        when(fileRepository.getAllFilesByIds(List.of("file1"))).thenReturn(List.of("file1Name"));


        List<String> fileNames = indexSearchService.searchBy("   mama   mila   ");

        assertEquals("Result should contain correct fileNames count", 1, fileNames.size());
        assertEquals("Result list should be correct",
                List.of("file1Name"), fileNames);

        verify(indexer, times(2)).findFilesIds(anyString());
        verify(fileRepository).getAllFilesByIds(anyCollection());

    }

    @Test
    public void searchByEmptyString() {
        List<String> fileNames = indexSearchService.searchBy("   ");

        assertEquals("Result should contain correct fileNames count", 0, fileNames.size());

        verify(indexer, times(0)).findFilesIds(anyString());
        verify(fileRepository, times(0)).getAllFilesByIds(anyCollection());
    }


    @Test
    public void searchByMultipleWords() {
        when(indexer.findFilesIds("mama")).thenReturn(List.of("file1"));
        when(indexer.findFilesIds("mila")).thenReturn(List.of("file1", "file2"));


        List<String> fileIds = indexSearchService.searchByMultipleWords(List.of("mama", "mila"));

        assertEquals("Result should contain correct fileNames count", 1, fileIds.size());
        assertEquals("Result list should be correct",
                List.of("file1"), fileIds);

        verify(indexer, times(2)).findFilesIds(anyString());
    }

    @Test
    public void searchByMultipleWordsNoIntersections() {
        when(indexer.findFilesIds("mama")).thenReturn(List.of("file1"));
        when(indexer.findFilesIds("mila")).thenReturn(List.of("file2"));


        List<String> fileIds = indexSearchService.searchByMultipleWords(List.of("mama", "mila"));

        assertEquals("Result should contain correct fileNames count", 0, fileIds.size());

        verify(indexer, times(2)).findFilesIds(anyString());
    }

    @Test
    public void searchByMultipleWordsNoResultOnFirstWord() {
        when(indexer.findFilesIds("mama")).thenReturn(Collections.emptyList());
        when(indexer.findFilesIds("mila")).thenReturn(List.of("file2"));


        List<String> fileIds = indexSearchService.searchByMultipleWords(List.of("mama", "mila"));

        assertEquals("Result should contain correct fileNames count", 0, fileIds.size());

        verify(indexer, times(2)).findFilesIds(anyString());
    }
}
