package com.homich.listener;

/**
 * Listener to react on file structure change on disk event
 */
public interface FileStructureChangedListener {
    void onFileStructureChanged();
}
