package com.homich;

import com.homich.repository.FileRepository;
import com.homich.repository.Indexer;
import com.homich.service.AppEventHandler;
import com.homich.service.FileService;
import com.homich.service.IndexPreparationService;
import com.homich.service.IndexSearchService;

public class ServiceRegistry {
    private final Indexer indexer;
    private final FileRepository fileRepository;
    private final AppEventHandler appEventHandler;

    private final IndexPreparationService indexPreparationService;
    private final IndexSearchService indexSearchService;
    private final FileService fileService;

    public ServiceRegistry() {
        this.indexer = Indexer.getInstance();
        this.fileRepository = FileRepository.getInstance();
        this.appEventHandler = AppEventHandler.getInstance();

        this.indexPreparationService = new IndexPreparationService();
        this.indexSearchService = new IndexSearchService(indexer, fileRepository);
        this.fileService = new FileService(fileRepository, indexPreparationService, indexer, appEventHandler);
    }

    public Indexer getIndexer() {
        return indexer;
    }

    public FileRepository getFileRepository() {
        return fileRepository;
    }

    public IndexPreparationService getIndexPreparationService() {
        return indexPreparationService;
    }

    public FileService getFileService() {
        return fileService;
    }

    public IndexSearchService getIndexSearchService() {
        return indexSearchService;
    }

    public AppEventHandler getAppEventHandler() {
        return appEventHandler;
    }
}
