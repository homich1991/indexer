package com.homich.ui;

import com.homich.listener.FileStructureChangedListener;
import com.homich.service.FileService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TreeItem;

import java.io.File;


public class SimpleFileTreeItem extends TreeItem<File> implements FileStructureChangedListener {

    private final SimpleFileTreeItem parentItem;
    private final SimpleFileTreeItemContextMenu menu;
    private final FileService fileService;


    public SimpleFileTreeItem(File rootFile, FileService fileService) {
        this(rootFile, fileService, null);
    }

    public SimpleFileTreeItem(File rootFile, FileService fileService, SimpleFileTreeItem parentItem) {
        super(rootFile);
        this.parentItem = parentItem;
        this.fileService = fileService;
        this.menu = new SimpleFileTreeItemContextMenu(rootFile, this, fileService);
    }


    @Override
    public ObservableList<TreeItem<File>> getChildren() {
        if (isFirstTimeChildren) {
            isFirstTimeChildren = false;
            super.getChildren().setAll(buildChildren(this));
        }
        return super.getChildren();
    }

    @Override
    public boolean isLeaf() {
        if (isFirstTimeLeaf) {
            isFirstTimeLeaf = false;
            isLeaf = getValue().isFile();
        }

        return isLeaf;
    }

    public void refreshChildren() {
        super.getChildren().removeAll(super.getChildren());
        super.getChildren().setAll(buildChildren(this));
    }


    private ObservableList<TreeItem<File>> buildChildren(TreeItem<File> treeItem) {
        this.getChildren().remove(0, this.getChildren().size());
        File f = treeItem.getValue();
        if (f != null && f.isDirectory()) {
            File[] files = f.listFiles();
            if (files != null) {
                ObservableList<TreeItem<File>> children = FXCollections
                        .observableArrayList();

                for (File childFile : files) {
                    children.add(new SimpleFileTreeItem(childFile, this.fileService, this));
                }

                return children;
            }

        }

        return FXCollections.emptyObservableList();
    }

    public ContextMenu getMenu() {
        return this.menu;
    }

    SimpleFileTreeItem getParentItem() {
        return parentItem;
    }

    private boolean isFirstTimeChildren = true;
    private boolean isFirstTimeLeaf = true;
    private boolean isLeaf;

    @Override
    public void onFileStructureChanged() {
        this.refreshChildren();
    }
}
