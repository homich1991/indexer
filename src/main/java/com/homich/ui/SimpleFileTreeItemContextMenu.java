package com.homich.ui;

import com.homich.service.FileService;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Optional;

@Slf4j
public class SimpleFileTreeItemContextMenu extends ContextMenu {

    public SimpleFileTreeItemContextMenu(File file,
                                         SimpleFileTreeItem simpleFileTreeItem,
                                         FileService fileService) {
        super();
        if (file == null) {
            return;
        }
        if (file.isDirectory()) {
            MenuItem addFile = new MenuItem("Add file");
            addFile.setOnAction(event -> {
                File selectedFile = fileChooser().showOpenDialog(getWindows().get(0));
                fileService.addFile(selectedFile, file);
                simpleFileTreeItem.refreshChildren();
            });

            MenuItem createDirectory = new MenuItem("Create directory");
            createDirectory.setOnAction(event -> {
                TextInputDialog td = new TextInputDialog("newDir");
                td.setHeaderText("Enter directory name");
                Optional<String> directoryName = td.showAndWait();
                if (td.getResult() != null) {
                    fileService.createDirectory(file, directoryName.orElse("newDir"));
                    simpleFileTreeItem.refreshChildren();
                }

            });

            MenuItem deleteDirectory = new MenuItem("Delete directory");
            deleteDirectory.setOnAction(getDeleteEventEventHandler(simpleFileTreeItem, fileService));

            this.getItems().addAll(addFile, createDirectory);
            if (simpleFileTreeItem.getParentItem() != null) {
                this.getItems().add(deleteDirectory);
            }
            return;
        }

        MenuItem deleteFile = new MenuItem("Delete file");
        deleteFile.setOnAction(getDeleteEventEventHandler(simpleFileTreeItem, fileService));

        this.getItems().addAll(deleteFile);
    }

    private FileChooser fileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt")
        );
        return fileChooser;
    }

    private EventHandler<ActionEvent> getDeleteEventEventHandler(SimpleFileTreeItem simpleFileTreeItem, FileService fileService) {
        return event -> {
            fileService.deleteFile(simpleFileTreeItem.getValue());
            simpleFileTreeItem.getParentItem().refreshChildren();
        };
    }
}
