package com.homich.ui;

import javafx.scene.control.TreeCell;

import java.io.File;

public class SimpleTreeCell extends TreeCell<File> {

    @Override
    public void updateItem(File item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            setText(getItem() == null ? "" : getItem().getName());
            setGraphic(getTreeItem().getGraphic());
            setContextMenu((((SimpleFileTreeItem) getTreeItem()).getMenu()));
        }
    }
}
