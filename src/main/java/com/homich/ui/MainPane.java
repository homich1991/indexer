package com.homich.ui;

import com.homich.ServiceRegistry;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.io.File;
import java.util.List;

public class MainPane extends HBox {

    public MainPane(ServiceRegistry serviceRegistry){
        super();

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(12);

        HBox searchPane = new HBox();
        searchPane.setSpacing(10.0);
        TextField searchField = new TextField();
        TextArea textArea = new TextArea();

        Button btnSearch = new Button("Search");
        searchPane.getChildren().addAll(searchField, btnSearch);
        btnSearch.setOnMouseClicked(event -> {
            String text = searchField.getText();
            if (text != null && !text.isBlank()) {
                List<String> strings = serviceRegistry.getIndexSearchService().searchBy(text);
                showStringsInTextArea(strings, textArea);
            }
        });


        grid.add(searchPane, 0, 0);
        grid.add(textArea, 0, 1);

        SimpleFileTreeItem rootTreeItem = new SimpleFileTreeItem(new File("index"),
                serviceRegistry.getFileService());
        serviceRegistry.getAppEventHandler().subscribe(rootTreeItem);

        TreeView<File> fileView = new TreeView<>(
                rootTreeItem);

        fileView.setCellFactory(p -> new SimpleTreeCell());


        this.getChildren().addAll(fileView, grid);
    }

    private void showStringsInTextArea(List<String> strings, TextArea textArea) {
        if (strings == null || strings.size() == 0) {
            textArea.setText("There are no files with this word");
            return;
        }
        textArea.setText(String.join("\n", strings));
    }

}
