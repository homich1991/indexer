package com.homich;

import com.homich.ui.MainPane;
import com.homich.worker.DiskScanner;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainFX extends Application {

    private final ServiceRegistry serviceRegistry;

    public MainFX() {
        this.serviceRegistry = new ServiceRegistry();
        serviceRegistry.getFileService().scanIndexDirectory();

        Thread t = new Thread(new DiskScanner(serviceRegistry));
        t.start();
    }

    @Override
    public void start(Stage stage) {
        Scene scene = new Scene(new MainPane(serviceRegistry), 640, 480);
        stage.setTitle("Indexer");
        stage.setScene(scene);
        stage.show();
    }


    public static void run(String[] args) {
        launch();
    }
}
