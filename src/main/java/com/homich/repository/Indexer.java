package com.homich.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Main indexer class
 * <p>
 * In this class we store index and process all operations over it
 */
public class Indexer {
    protected final ConcurrentHashMap<Byte, ConcurrentHashMap<String, Set<String>>> index;

    private static final Indexer INSTANCE = new Indexer();

    public static Indexer getInstance() {
        return INSTANCE;
    }

    protected Indexer() {
        this.index = new ConcurrentHashMap<>();
    }


    /**
     * Add prepared words (or pattern) to files map to the index
     * @param wordsToFilesMap prepared map word -> Set(file Ids)
     */
    public void addToIndex(Map<String, Set<String>> wordsToFilesMap) {
        Map<Byte, Map<String, Set<String>>> hashedMap = generateHashes(wordsToFilesMap);
        addToIndexHashed(hashedMap);
    }

    /**
     * Searches files ids in index by provided word
     * @param word one word (pattern) trimmed
     * @return List of files ids which contains provided word(pattern)
     */
    public List<String> findFilesIds(String word) {
        word = word.toLowerCase();
        byte hashCode = hashGenerator(word);
        ConcurrentHashMap<String, Set<String>> wordsToFilesMap = index.get(hashCode);
        if (wordsToFilesMap == null || !wordsToFilesMap.containsKey(word)) {
            return new ArrayList<>();
        }
        return new ArrayList<>(wordsToFilesMap.get(word));

    }

    /**
     * Removes filesIds and connected to them words from index
     * @param filesIds ids of files which needed to be deleted
     */
    public synchronized void removeFiles(List<String> filesIds) {
        for (Byte hashKey : index.keySet()) {
            ConcurrentHashMap<String, Set<String>> wordToFilesMap = index.get(hashKey);
            for (String word : wordToFilesMap.keySet()) {
                wordToFilesMap.get(word).removeAll(filesIds);
            }
        }
    }

    //Method to add prepared map with hashes to index
    private void addToIndexHashed(Map<Byte, Map<String, Set<String>>> indexToAdd) {
        for (Byte hash : indexToAdd.keySet()) {
            if (!index.containsKey(hash)) {
                index.put(hash, new ConcurrentHashMap<>(indexToAdd.get(hash)));
                continue;
            }

            ConcurrentHashMap<String, Set<String>> mapWordToFileIdList = index.get(hash);
            Map<String, Set<String>> subIndexToAdd = indexToAdd.get(hash);

            for (String word : subIndexToAdd.keySet()) {
                if (!mapWordToFileIdList.containsKey(word)) {
                    mapWordToFileIdList.put(word, subIndexToAdd.get(word));
                    continue;
                }
                Set<String> fileList = new HashSet<>(mapWordToFileIdList.get(word));
                fileList.addAll(subIndexToAdd.get(word));
                mapWordToFileIdList.put(word, fileList);
            }

        }
    }

    //Method to prepare map to be added to the index. Extend provided map with additional layer with hashes by word.
    private Map<Byte, Map<String, Set<String>>> generateHashes(Map<String, Set<String>> wordsToFilesMap) {
        Map<Byte, Map<String, Set<String>>> result = new HashMap<>();
        for (String word : wordsToFilesMap.keySet()) {
            byte hashCode = hashGenerator(word);
            if (!result.containsKey(hashCode)) {
                result.put(hashCode, new HashMap<>());
            }
            result.get(hashCode).put(word, wordsToFilesMap.get(word));
        }
        return result;
    }

    //Hash generator. 32 was taken as a divider by default for bucket choose.
    // Could be enhanced with smarter way of bucket separation.
    protected byte hashGenerator(String word) {
        return (byte) (word.hashCode() % 32);
    }
}
