package com.homich.repository;

import com.homich.entity.FileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * File Repository class
 *
 * Holds information about files and filesInfo which were processed by index
 */
@Slf4j
public class FileRepository {
    protected final ConcurrentHashMap<String, File> fileStorage;
    protected final ConcurrentHashMap<String, FileInfo> fileNameToInfo;

    private static final FileRepository INSTANCE = new FileRepository();

    public static FileRepository getInstance() {
        return INSTANCE;
    }

    protected FileRepository() {
        this.fileStorage = new ConcurrentHashMap<>();
        this.fileNameToInfo = new ConcurrentHashMap<>();
    }

    /**
     * Stores file and calculate fileInfo or return fileId if file was already stored
     * @param file file to store
     * @return fileId
     */
    public String storeFile(File file) {
        String absolutePath = file.getAbsolutePath();
        if (fileNameToInfo.containsKey(absolutePath)) {
            return fileNameToInfo.get(absolutePath).getId();
        }

        String fileId = UUID.randomUUID().toString();
        fileStorage.put(fileId, file);
        fileNameToInfo.put(absolutePath, new FileInfo(fileId, absolutePath, calculateMd5(file)));
        return fileId;
    }

    /**
     * Removes files from storage
     * @param filesIds list of Ids which should be removed
     */
    public synchronized void removeFiles(List<String> filesIds) {
        for (String filesId : filesIds) {
            File remove = fileStorage.remove(filesId);
            fileNameToInfo.remove(remove.getAbsolutePath());
        }
    }

    /**
     * Returns list of fileNames in storage by fileIds
     * @param ids file ids
     * @return list of file names(absolute paths)
     */
    public List<String> getAllFilesByIds(Collection<String> ids) {
        return ids.stream()
                .map(fileStorage::get)
                .filter(Objects::nonNull)
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());
    }

    /**
     * Returns list of fileIds by fileNames(absolute paths)
     * @param fileNames file names
     * @return list of file Ids.
     */
    public List<String> getFileIdsByFileName(Collection<String> fileNames) {
        return fileNames.stream()
                .map(fileNameToInfo::get)
                .map(FileInfo::getId)
                .collect(Collectors.toList());
    }

    /**
     *
     * @return Names(absolute paths) of all files in index
     */
    public Set<String> getAllFilesInIndex() {
        return new HashSet<>(fileNameToInfo.keySet());
    }

    /**
     * Returns stored file info of file by file name(absolute path)
     * @param fileName name (absolute path) of file
     * @return stored file info record
     */
    public FileInfo getFileInfo(String fileName) {
        return fileNameToInfo.get(fileName);
    }

    /**
     * Calculate md5 checksum based on file inners
     * @param file file for checksum calculation
     * @return md5 checksum
     */
    public String calculateMd5(File file) {
        try (InputStream is = Files.newInputStream(Paths.get(file.getAbsolutePath()))) {
            return DigestUtils.md5Hex(is);

        } catch (IOException e) {
            log.error("Error on digest calculation");
        }
        return "";
    }
}
