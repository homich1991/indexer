package com.homich.worker;

import com.homich.ServiceRegistry;

/**
 * Job which initiates index root directory rescan
 */
public class DiskScanner implements Runnable{

    private final ServiceRegistry serviceRegistry;

    public DiskScanner(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            serviceRegistry.getFileService().reScanIndexDirectory();
        }
    }
}
