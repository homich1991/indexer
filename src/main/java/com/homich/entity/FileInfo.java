package com.homich.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Entity to store file information like
 *  - id
 *  - absolute path
 *  - md5 checksum
 */
@Getter
@RequiredArgsConstructor
public class FileInfo {
    private final String id;
    private final String fileAbsolutePath;
    private final String md5sum;

}
