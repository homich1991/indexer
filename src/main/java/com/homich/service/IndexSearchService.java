package com.homich.service;

import com.homich.repository.FileRepository;
import com.homich.repository.Indexer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Service which performs search in the index
 */
public class IndexSearchService {
    private final Indexer indexer;
    private final FileRepository fileRepository;

    public IndexSearchService(Indexer indexer, FileRepository fileRepository) {
        this.indexer = indexer;
        this.fileRepository = fileRepository;
    }

    /**
     * Search fileNames by provided text
     * @param text some text to search in the index. Could contain one or several words.
     * @return files which contains all words from provided text
     */
    public List<String> searchBy(String text) {
        List<String> words = Arrays.stream(text.split(" "))
                .map(String::trim)
                .filter(word -> !word.isBlank())
                .collect(Collectors.toList());
        if (words.size() == 0) {
            return new ArrayList<>();
        }
        List<String> filesIds;
        if (words.size() == 1) {
            filesIds = indexer.findFilesIds(words.get(0));
        } else {
            filesIds = searchByMultipleWords(words);
        }
        return fileRepository.getAllFilesByIds(filesIds);
    }

    //Inner method to search by multiple words
    protected List<String> searchByMultipleWords(List<String> words) {
        Set<String> filesIds = null;
        List<HashSet<String>> results = words.stream()
                .map(indexer::findFilesIds)
                .map(HashSet::new)
                .collect(Collectors.toList());


        for (HashSet<String> result : results) {
            if (filesIds == null) {
                filesIds = result;
                continue;
            }
            if (filesIds.isEmpty()) {
                break;
            }
            filesIds.retainAll(result);
        }

        if (filesIds == null || filesIds.isEmpty()) {
            return new ArrayList<>();
        }
        return new ArrayList<>(filesIds);
    }

}
