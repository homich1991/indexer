package com.homich.service;

import com.homich.listener.FileStructureChangedEvent;
import com.homich.listener.FileStructureChangedListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Simple event handler to control listeners and events
 */
public class AppEventHandler {
    List<FileStructureChangedListener> fileStructureChangedListeners;

    private static final AppEventHandler INSTANCE = new AppEventHandler();

    public static AppEventHandler getInstance() {
        return INSTANCE;
    }

    private AppEventHandler() {
        this.fileStructureChangedListeners = Collections.synchronizedList(new ArrayList<>());
    }

    public void subscribe(FileStructureChangedListener listener) {
        this.fileStructureChangedListeners.add(listener);
    }

    public void unsubscribe(FileStructureChangedListener listener) {
        this.fileStructureChangedListeners.remove(listener);
    }

    public void fire(FileStructureChangedEvent event) {
        fileStructureChangedListeners.forEach(FileStructureChangedListener::onFileStructureChanged);
    }
}
