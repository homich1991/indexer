package com.homich.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service to prepare map words -> fileNames from files before it could be added to the index
 */
@Slf4j
public class IndexPreparationService {

    /**
     * Prepare index map for one file
     * @param fileId file id received from {@link com.homich.repository.FileRepository}
     * @param file file to index
     * @return prepared map word -> fileNames (absolute path)
     */
    public Map<String, Set<String>> prepareIndexForFile(String fileId, File file) {
        return indexFileByWordsWithoutHashing(fileId, file);

    }

    /**
     * Prepare index map for multiple files
     * @param filesWithIds list of pairs (fileId -> file) to index.
     *                     fileId -> id received from {@link com.homich.repository.FileRepository}.
     *                     file -> file to index
     * @return prepared map word -> fileNames (absolute path)
     */
    public Map<String, Set<String>> prepareIndexForFiles(List<Pair<String, File>> filesWithIds) {
        Map<String, Set<String>> wordsToFilesResult = new HashMap<>();
        for (Pair<String, File> pair : filesWithIds) {
            mergeMap(wordsToFilesResult, indexFileByWordsWithoutHashing(pair.getKey(), pair.getValue()));
        }
        return wordsToFilesResult;

    }

    //Inner method to merge several maps prepared for be added to index
    private void mergeMap(Map<String, Set<String>> resultMap, Map<String, Set<String>> mapToMerge) {
        for (String word : mapToMerge.keySet()) {
            if (!resultMap.containsKey(word)) {
                resultMap.put(word, mapToMerge.get(word));
                continue;
            }
            resultMap.get(word).addAll(mapToMerge.get(word));
        }
    }


    //Inner method which prepare index of file by words
    //This place could be extended with indexing by lectures or other patterns
    private Map<String, Set<String>> indexFileByWordsWithoutHashing(String fileId, File file) {
        List<String> lines;
        try {
            lines = Files.readAllLines(file.toPath());
        } catch (IOException e) {
            log.error("Couldn't index file {}", file.getAbsolutePath());
            return new HashMap<>();
        }
        Set<String> wordsFromFile = new HashSet<>();

        for (String line : lines) {
            wordsFromFile.addAll(
                    Arrays.stream(line.split(" "))
                            .map(String::trim)
                            .filter(word -> !word.isBlank())
                            .map(String::toLowerCase)
                            .collect(Collectors.toList())
            );
        }
        return wordsFromFile.stream()
                .collect(Collectors.toMap(
                        word -> word,
                        word -> {
                            HashSet<String> hashSet = new HashSet<>();
                            hashSet.add(fileId);
                            return hashSet;
                        }
                ));
    }
}
