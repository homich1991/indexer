package com.homich.service;

import com.homich.entity.FileInfo;
import com.homich.listener.FileStructureChangedEvent;
import com.homich.repository.FileRepository;
import com.homich.repository.Indexer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service to control all operations on files
 */
@Slf4j
public class FileService {
    private final FileRepository fileRepository;
    private final IndexPreparationService indexPreparationService;
    private final Indexer indexer;
    private final AppEventHandler appEventHandler;

    private final static Path INDEX_DIRECTORY_PATH = Path.of("index");
    private final File indexDirectory;

    public FileService(FileRepository fileRepository,
                       IndexPreparationService indexPreparationService,
                       Indexer indexer,
                       AppEventHandler appEventHandler) {
        this.fileRepository = fileRepository;
        this.indexPreparationService = indexPreparationService;
        this.indexer = indexer;
        this.appEventHandler = appEventHandler;

        this.indexDirectory = new File(INDEX_DIRECTORY_PATH.toUri());
    }

    /**
     * Store file in repository and add to the index
     * @param file file to index
     */
    public void indexFile(File file) {

        Map<String, Set<String>> wordsToFilesMap = indexPreparationService.prepareIndexForFile(fileRepository.storeFile(file), file);
        indexer.addToIndex(wordsToFilesMap);
    }

    /**
     * Stores all files in repository and add all to the index
     * @param files files to index
     */
    public void indexFiles(List<File> files) {
        List<Pair<String, File>> filesWithIds = files.stream()
                .map(file -> new ImmutablePair<>(fileRepository.storeFile(file), file))
                .collect(Collectors.toList());
        Map<String, Set<String>> wordsToFilesResult = indexPreparationService.prepareIndexForFiles(filesWithIds);
        indexer.addToIndex(wordsToFilesResult);
    }

    /**
     * Method to run on application startup to scan index and create index root directory if not exists
     */
    public void scanIndexDirectory() {
        if (!indexDirectory.exists() || !indexDirectory.isDirectory()) {
            try {
                Files.createDirectory(INDEX_DIRECTORY_PATH);
            } catch (IOException e) {
                log.error("Couldn't create index directory", e);
            }
        }

        indexFiles(getAllFilesFromDirectory(indexDirectory));
    }


    /**
     * Method to run periodically to rescan index root directory to catch changes in it
     */
    public void reScanIndexDirectory() {
        log.info("Index re-scan started");
        List<File> allFilesFromDirectory = getAllFilesFromDirectory(indexDirectory);
        Set<String> filesInIndex = fileRepository.getAllFilesInIndex();

        ChangesBetweenFSAndIndex changes = checkChanges(allFilesFromDirectory, filesInIndex);

        //Files which left after processing not exists in the filesystem, so should be deleted from index
        Set<String> toRemoveFromIndex = new HashSet<>();
        toRemoveFromIndex.addAll(changes.getDeleted());
        //Before reindexing we also should delete existing files
        toRemoveFromIndex.addAll(changes.getUpdated().stream().map(File::getAbsolutePath).collect(Collectors.toSet()));
        removeFromIndex(this.fileRepository.getFileIdsByFileName(toRemoveFromIndex));

        List<File> toReIndex = new ArrayList<>();
        toReIndex.addAll(changes.getCreated());
        toReIndex.addAll(changes.getUpdated());

        indexFiles(toReIndex);

        int created = changes.getCreated().size();
        int updated = changes.getUpdated().size();
        int deleted = changes.getDeleted().size();

        if (created > 0 || deleted > 0) {
            appEventHandler.fire(new FileStructureChangedEvent());
        }

        log.info("Index re-scan finished. {} were created. {} were updated. {} were deleted", created, updated, deleted);
    }

    /**
     * Adds new file to the system
     * @param selectedFile file to add
     * @param directory directory where to add file
     */
    public void addFile(File selectedFile, File directory) {
        try {
            FileUtils.copyFileToDirectory(selectedFile, directory);
            indexFile(new File(directory, selectedFile.getName()));
        } catch (IOException e) {
            log.error("Unable to add file");
        }
    }

    /**
     * Deletes file from the system
     * @param file file to delete
     */
    public void deleteFile(File file) {
        if (file == null) {
            return;
        }

        List<String> fileNames;
        if (file.isDirectory()) {
            fileNames = getAllFilesFromDirectory(file)
                    .stream()
                    .map(File::getAbsolutePath)
                    .collect(Collectors.toList());
        } else {
            fileNames = List.of(file.getAbsolutePath());
        }


        List<String> filesIds = this.fileRepository.getFileIdsByFileName(fileNames);
        try {
            FileUtils.forceDelete(file);
            removeFromIndex(filesIds);
        } catch (IOException e) {
            log.error("Couldn't delete directory", e);
        }
    }

    /**
     * Method to create new directory
     * @param parentDirectory parent directory where to add new
     * @param directoryName new directory name
     */
    public void createDirectory(File parentDirectory, String directoryName) {
        try {
            Files.createDirectory(Paths.get(parentDirectory.getPath() + File.separator + directoryName));
        } catch (IOException e) {
            log.error("Couldn't create directory", e);
        }
    }

    //Inner method to remove files from storage and from index
    private void removeFromIndex(List<String> filesIds) {
        indexer.removeFiles(filesIds);
        fileRepository.removeFiles(filesIds);
    }



    //Inner method to retrieve all files from directory recursively
    private List<File> getAllFilesFromDirectory(File directory) {
        return new ArrayList<>(
                FileUtils.listFiles(directory, new SuffixFileFilter(".txt"), TrueFileFilter.TRUE)
        );
    }

    //Inner method to catch changes between file system and files stored in index
    ChangesBetweenFSAndIndex checkChanges(List<File> allFilesFromDirectory, Set<String> filesInIndex) {
        List<File> toIndexCreated = new ArrayList<>();
        List<File> toIndexUpdated = new ArrayList<>();

        for (File file : allFilesFromDirectory) {
            String absolutePath = file.getAbsolutePath();
            if (!filesInIndex.contains(absolutePath)) {
                //If index doesn't contain file which exists in filesystem - file should be added to index
                toIndexCreated.add(file);
            } else {
                FileInfo fileInfo = fileRepository.getFileInfo(absolutePath);
                String digest = fileRepository.calculateMd5(file);
                if (!fileInfo.getMd5sum().equals(digest)) {
                    //If md5 sum of file in index and in filesystem differs - file was changed and should be re-indexed
                    toIndexUpdated.add(file);
                }
                //Remove processed files from set to determine files to delete from index
                filesInIndex.remove(absolutePath);
            }
        }
        return new ChangesBetweenFSAndIndex(toIndexCreated, toIndexUpdated, filesInIndex);
    }


    //Inner entity to hold changed encountered on file system
    @Getter
    @RequiredArgsConstructor
    class ChangesBetweenFSAndIndex {
        private final List<File> created;
        private final List<File> updated;
        private final Set<String> deleted;
    }
}
